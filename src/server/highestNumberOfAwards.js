const fs = require("fs");
const convertCsvToJson = require("../convertCsvToJson");
const csvFilePath = "../data/matches.csv";

function highestNumberOfAwards() {
  try {
    convertCsvToJson(csvFilePath).then((jsonData) => {
      const highestNumberOfAwards = {};
      const result = {};

      for (let index = 0; index < jsonData.length; index++) {
        const match = jsonData[index];
        const year = match.season;
        const player = match.player_of_match;

        if (highestNumberOfAwards[year]) {
          if (highestNumberOfAwards[year][player]) {
            highestNumberOfAwards[year][player]++;
          } else {
            highestNumberOfAwards[year][player] = 1;
          }
        } else {
          highestNumberOfAwards[year] = {};
        }
      }

      // console.log(highestNumberOfAwards)

      for (let year in highestNumberOfAwards) {
        let maxCount = 0;
        let maxPlayer = "";
        for (let myplayer in highestNumberOfAwards[year]) {
          if (highestNumberOfAwards[year][myplayer] > maxCount) {
            maxCount = highestNumberOfAwards[year][myplayer];
            maxPlayer = myplayer;
          }
        }
        result[year] = maxPlayer;
      }

      fs.writeFile(
        "../public/output/highestNumberOfAwards.json",
        JSON.stringify(result),
        (err) => {
          if (err) {
            console.error(err);
          } else {
            console.log("File has been created");
          }
        }
      );
      console.log(result);
    });
  } catch (error) {
    console.error("Error:", error);
  }
}

highestNumberOfAwards();
