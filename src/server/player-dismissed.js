const fs = require("fs");
const convertCsvToJson = require("../convertCsvToJson");
const csvFilePath = "../data/deliveries.csv";

//Find the highest number of times one player has been dismissed by another player
function playerDismissed() {
  try {
    convertCsvToJson(csvFilePath).then((jsonData) => {
      let dismissedPlayerObj = {};
      let maxDimissals = 0;
      let maxPlayer = "";

      for (let index = 0; index < jsonData.length; index++) {
        if (jsonData[index].player_dismissed != "") {
          let dismissedPlayerName = jsonData[index].player_dismissed;
          let bowler = jsonData[index].bowler;
          let nestedKey = `${dismissedPlayerName}-${bowler}`;
          // if(dismissedPlayerObj[dismissedPlayerName])
          // {
          //     dismissedPlayerObj[dismissedPlayerName]++;
          // }
          // else{
          // dismissedPlayerObj[dismissedPlayerName]={};
          // }

          if (dismissedPlayerObj[nestedKey]) {
            dismissedPlayerObj[nestedKey]++;
          } else {
            dismissedPlayerObj[nestedKey] = 1;
          }
        }
      }
      // console.log(dismissedPlayerObj);
      for (player in dismissedPlayerObj) {
        if (dismissedPlayerObj[player] > maxDimissals) {
          maxDimissals = dismissedPlayerObj[player];
          maxPlayer = player;
        }
      }

      // console.log(maxDimissals+" "+maxPlayer);
      console.log(maxPlayer.split("-")[0]);
      let result = maxPlayer.split("-")[0];

      fs.writeFile(
        "../public/output/PlayerDismissed.json",
        JSON.stringify(result),
        (err) => {
          if (err) {
            console.error(err);
          } else {
            console.log("File has been created");
          }
        }
      );
      //   console.log(matchesPerYear);
    });
  } catch (error) {
    console.error("Error:", error);
  }
}

playerDismissed();
