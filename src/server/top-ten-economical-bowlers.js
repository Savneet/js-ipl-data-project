const fs = require("fs");
const convertCsvToJson = require("../convertCsvToJson");
const matchCsvFilePath = "../data/matches.csv";
const deliveryCsvFilePath = "../data/deliveries.csv";

function topTenEconomicalBowlers() {
  try {
    convertCsvToJson(matchCsvFilePath).then((matchJsonData) => {
      convertCsvToJson(deliveryCsvFilePath).then((deliveryJsonData) => {
        const matches2015 = []; //Finding matches that happened in 2015
        for (let i = 0; i < matchJsonData.length; i++) {
          if (matchJsonData[i].season == 2015) {
            matches2015.push(matchJsonData[i]);
          }
        }

        //console.log(matches2015);
        const bowlers = {};

        // Looping through the deliveries and calculate the economy for each bowler
        for (let i = 0; i < deliveryJsonData.length; i++) {
          const delivery = deliveryJsonData[i];
          const matchId = delivery.match_id;
          const bowlingTeam = delivery.bowling_team;
          const bowler = delivery.bowler;
          const totalRuns = parseInt(delivery.total_runs);

          //   console.log("Delivery");
          //   console.log(delivery);
          // Check if the match is from 2015
          let match = null;
          for (let j = 0; j < matches2015.length; j++) {
            if (matches2015[j].id === matchId) {
              match = matches2015[j];
              break;
            }
          }

          if (match) {
            if (!bowlers[bowler]) {
              bowlers[bowler] = {
                team: bowlingTeam,
                runs: totalRuns,
                balls: 1,
              };
            } else {
              bowlers[bowler].runs += totalRuns;
              bowlers[bowler].balls += 1;
            }
          }
        }
        //console.log(bowlers);

        // Calculating the economy for each bowler
        const bowlersArray = [];
        for (const bowler in bowlers) {
          const data = bowlers[bowler];
          const overs = Math.floor(data.balls / 6); //There are 6 balls in an over
          const remainingBalls = data.balls % 6;
          const economy = data.runs / (overs + remainingBalls / 6); //economy rate is runs/overs
          bowlersArray.push({ bowler, team: data.team, economy });
        }
        // console.log(bowlersArray);

        // Sorting the bowlers by economy and get the top 10 using slice method
        bowlersArray.sort((a, b) => a.economy - b.economy);
        const topBowlers = bowlersArray.slice(0, 10);
        // console.log(topBowlers);

        // Printing the top 10 economical bowlers
        console.log("Top 10 Economical Bowlers in 2015:");
        for (let i = 0; i < topBowlers.length; i++) {
          const { bowler, team, economy } = topBowlers[i];
          console.log(
            `${i + 1}. ${bowler} (${team}) - Economy: ${economy.toFixed(2)}`
          );
        }
        fs.writeFile(
          "../public/output/topTenEconomicalBowlers.json",
          JSON.stringify(
            topBowlers
          ),
          (err) => {
            if (err) {
              console.error(err);
            } else {
              console.log("File has been created");
            }
          }
        );
        
      });
    });
  } catch (error) {
    console.error("Error:", error);
  }
}
topTenEconomicalBowlers();
//In cricket, the term "economical" refers to a bowler who concedes fewer runs per over,
//which is considered desirable for a bowler's performance.
//Therefore, when we talk about "top economical bowlers," we are looking for bowlers with the lowest economy rates
//So, when we talk about a bowler conceding fewer runs, it means that the bowler is not
//allowing the batsmen to score many runs off their bowling, indicating effective bowling performance.
