const fs = require("fs");
const convertCsvToJson = require("../convertCsvToJson");
const matchCsvFilePath = "../data/matches.csv";
const deliveryCsvFilePath = "../data/deliveries.csv";

//   const csvData = fs.readFileSync(csvFilePath, "utf8");

function handleMatchesData() {
  try {
    convertCsvToJson(matchCsvFilePath).then((matchJsonData) => {
      convertCsvToJson(deliveryCsvFilePath).then((deliveryJsonData) => {
        const extraRunsPerTeam = {};

        for (let index = 0; index < matchJsonData.length; index++) {
          const match = matchJsonData[index];
          const year = match["season"];
          if (year == 2016) {
            let id = match["id"];
            for (let index2 = 0; index2 < deliveryJsonData.length; index2++) {
              const match2 = deliveryJsonData[index2];
              let deliveryId = match2["match_id"];
              if (id == deliveryId) {
                const batting_team = match2["batting_team"];
                const extra_runs = parseInt(match2["extra_runs"]);
                if (extraRunsPerTeam[batting_team]) {
                  extraRunsPerTeam[batting_team] += extra_runs;
                } else {
                  extraRunsPerTeam[batting_team] = extra_runs;
                }
              }
            }
          }
        }

        fs.writeFile(
          "../public/output/extraRunsConceded.json",
          JSON.stringify(extraRunsPerTeam),
          (err) => {
            if (err) {
              console.error(err);
            } else {
              console.log("File has been created");
            }
          }
        );
        console.log(extraRunsPerTeam);
      });
    });
  } catch (error) {
    console.error("Error:", error);
  }
}

//   convertCsvToJson(csvData).then((jsonData) => handleMatchesData(jsonData));

handleMatchesData();
