const fs = require("fs");
const convertCsvToJson = require("../convertCsvToJson");
const csvFilePath = "../data/matches.csv";

function wonTossAndMatch() {
  try {
    convertCsvToJson(csvFilePath).then((jsonData) => {
      const wonTossAndMatchTeam = {};

      for (let index = 0; index < jsonData.length; index++) {
        const match = jsonData[index];
        const tossWinner = match.toss_winner;
        const matchWinner = match.winner;

        if (matchWinner == tossWinner) {
            // console.log("Hello");
          if (wonTossAndMatchTeam[tossWinner]) {
            wonTossAndMatchTeam[tossWinner]++;
          } else {
            wonTossAndMatchTeam[tossWinner] = 1;
          }
        }
      }
      fs.writeFile(
        "../public/output/wonTossAndMatch.json",
        JSON.stringify(
          wonTossAndMatchTeam
        ),
        (err) => {
          if (err) {
            console.error(err);
          } else {
            console.log("File has been created");
          }
        }
      );
      console.log(wonTossAndMatchTeam);
    });
  } catch (error) {
    console.error("Error:", error);
  }
}

wonTossAndMatch();
