const fs = require("fs");
const convertCsvToJson = require("../convertCsvToJson");

const deliveriesFilePath = "../data/deliveries.csv";
const matchesFilePath = "../data/matches.csv";

//Find the strike rate of a batsman for each season

function calculateStrikeRate() {
  try {
    convertCsvToJson(matchesFilePath).then((matchesData) => {
        convertCsvToJson(deliveriesFilePath).then((deliveriesData) => {
      const result = {};

      // Creating a map of match IDs to seasons
      const matchIdToSeason = {};
      for (const match of matchesData) {
        matchIdToSeason[match.id] = match.season;
      }

      for (const delivery of deliveriesData) {
        const matchId = delivery.match_id;
        const batsman = delivery.batsman;
        const runs = parseInt(delivery.batsman_runs);
        
          const season = matchIdToSeason[matchId];

          if (!result[season]) {
            result[season] = {};
          }

          if (!result[season][batsman]) {
            result[season][batsman] = { runs: 0, balls: 0 };
          }

          result[season][batsman].runs += runs;
          result[season][batsman].balls++;
        }
    

      // Calculating strike rate for each batsman in each season
      for (const season in result) {
        for (const batsman in result[season]) {
          const { runs, balls } = result[season][batsman];
          const strikeRate = ((runs / balls) * 100);

          // Updating the result with the strike rate for the batsman
          result[season][batsman] = strikeRate.toFixed(2);
        }
      }
      fs.writeFile(
        "../public/output/strinkRate.json",
        JSON.stringify(result),
        (err) => {
          if (err) {
            console.error(err);
          } else {
            console.log("File has been created");
          }
        }
      );
      console.log(result);
    });
});
  } catch (error) {
    console.error("Error:", error);
  }
}

calculateStrikeRate();
