const fs = require("fs");
const convertCsvToJson = require("../convertCsvToJson");
const csvFilePath = "../data/matches.csv";

//   const csvData = fs.readFileSync(csvFilePath, "utf8");
function handleMatchesData() {
  try {
    convertCsvToJson(csvFilePath).then((jsonData) => {
      const matchesPerYear = {}; // Object to store the count of matches per year

      for (let index = 0; index < jsonData.length; index++) {
        const match = jsonData[index];
        const year = match["season"];
        if (matchesPerYear[year]) {
          matchesPerYear[year]++;
        } else {
          matchesPerYear[year] = 1;
        }
      }

      fs.writeFile(
        "../public/output/matchesPerYear.json",
        JSON.stringify(matchesPerYear),
        (err) => {
          if (err) {
            console.error(err);
          } else {
            console.log("File has been created");
          }
        }
      );
      console.log(matchesPerYear);
    });
  } catch (error) {
    console.error("Error:", error);
  }
}

//   convertCsvToJson(csvData).then((jsonData) => handleMatchesData(jsonData));

handleMatchesData();
