const fs = require("fs");
const convertCsvToJson = require("../convertCsvToJson");
const csvFilePath = "../data/matches.csv";

//   const csvData = fs.readFileSync(csvFilePath, "utf8");
function handleMatchesData() {
  try {
    convertCsvToJson(csvFilePath).then((jsonData) => {
      const matchesWonPerTeamPerYear = {};
      const nestedObj = {};

      for (let index = 0; index < jsonData.length; index++) {
        const match = jsonData[index];
        const year = match["season"];
        const winner = match["winner"];
        if (matchesWonPerTeamPerYear[year]) {
          if (nestedObj[winner]) {
            nestedObj[winner]++;
          } else {
            nestedObj[winner] = 1;
          }
        } else {
          matchesWonPerTeamPerYear[year] = nestedObj;
        }
      }

      fs.writeFile(
        "../public/output/matchesWonPerTeamPerYear.json",
        JSON.stringify(matchesWonPerTeamPerYear),
        (err) => {
          if (err) {
            console.error(err);
          } else {
            console.log("File has been created");
          }
        }
      );
      console.log(matchesWonPerTeamPerYear);
    });
  } catch (error) {
    console.error("Error:", error);
  }
}

//   convertCsvToJson(csvData).then((jsonData) => handleMatchesData(jsonData));

handleMatchesData();
