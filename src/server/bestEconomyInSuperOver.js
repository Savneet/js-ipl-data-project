const fs = require("fs");
const convertCsvToJson = require("../convertCsvToJson");
const csvFilePath = "../data/deliveries.csv";

//Find the bowler with the best economy in super overs
function bestEconomy() {
  try {
    convertCsvToJson(csvFilePath).then((jsonData) => {
      const bestEconomyObj = {};

      for (let index = 0; index < jsonData.length; index++) {
        if (jsonData[index].is_super_over == 1) {
          let bowler = jsonData[index].bowler;
          let runs = parseInt(jsonData[index].total_runs);
          if (!bestEconomyObj[bowler]) {
            bestEconomyObj[bowler] = {
              runs: runs,
              balls: 1,
            };
          } else {
            bestEconomyObj[bowler].runs += runs;
            bestEconomyObj[bowler].balls += 1;
          }
        }
      }
    //   console.log(bestEconomyObj);
      const bowlersArray = [];
      for (const bowler in bestEconomyObj) {
        const data = bestEconomyObj[bowler];
        const overs = Math.floor(data.balls / 6); //There are 6 balls in an over
        // const remainingBalls = data.balls % 6;
        const economy = data.runs / (overs); //economy rate is runs/overs
        bowlersArray.push({ bowler, economy });
      }
    //   console.log(bowlersArray);

      bowlersArray.sort((a, b) => a.economy - b.economy);
      let player=bowlersArray[0];
      console.log(player);
    
    fs.writeFile(
      "../public/output/bestEconomyInSuperOver.json",
      JSON.stringify(player.bowler),
      (err) => {
        if (err) {
          console.error(err);
        } else {
          console.log("File has been created");
        }
      }
    );
});
} catch (error) {
  console.error("Error:", error);
}
}
bestEconomy();
