function convertCsvToJson(csvFilePathVar)
{
    const csvFilePath=csvFilePathVar;
    const csv=require('csvtojson');
    return csv()
    .fromFile(csvFilePath)
    .then((jsonObj)=>{
        // console.log(jsonObj);
        return jsonObj;
        
    });
    
}

module.exports=convertCsvToJson;

